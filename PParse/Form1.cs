﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Generic;
//using Excel = Microsoft.Office.Interop.Excel;

namespace FSTECParser
{


    public partial class Form1 : Form
    {

        public static List<Threat> threatlist2 = new List<Threat>();
        public static List<Threat> threatlist3 = new List<Threat>();
        public static Dictionary<int, Threat> Threatdic = new Dictionary<int, Threat>();
        public static Dictionary<int, Threat> Threatdic1 = new Dictionary<int, Threat>();
        public Form1()
        {
            InitializeComponent();
            SetHeight(listView3, 100); SetHeight(listView2, 100);
            
        }

        public static void ThreatParse2(string fileName, Dictionary<int, Threat> l)
        {
            try
            {
                DateTime start = DateTime.Now;
                using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
                {
                    var myWorksheet = xlPackage.Workbook.Worksheets.First();
                    var totalRows = myWorksheet.Dimension.End.Row;
                    var totalColumns = myWorksheet.Dimension.End.Column;

                    for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                    {

                        var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());

                        l.Add(Convert.ToInt32(row.ElementAt(0).ToString()), new Threat(Convert.ToInt32(row.ElementAt(0).ToString()), row.ElementAt(1).ToString(), row.ElementAt(2).ToString(), row.ElementAt(3).ToString(), row.ElementAt(4).ToString(), row.ElementAt(5).ToString(), row.ElementAt(6).ToString(), row.ElementAt(7).ToString()));
                    }

                }
            }
            catch (Exception c) { MessageBox.Show(c.Message); }

            MessageBox.Show(l.Count.ToString() + "- Количество угрозв  в базе");
        }


        private void toList()
        {
            foreach (KeyValuePair<int, Threat> box in Threatdic)
            {
                try
                {

                    ListViewItem itm = new ListViewItem(Threat.toarrr(box.Value));
                    listView1.Items.Add(itm);
                    string[] v = new string[2] { Threat.toarrr(box.Value)[0], Threat.toarrr(box.Value)[1] };
                    listView2.Items.Add(new ListViewItem(v));
                }
                catch (Exception x) { MessageBox.Show(x.Message); }
            }
        }


        private void SetHeight(ListView listView, int height)
        {
            ImageList imgList = new ImageList();
            imgList.ImageSize = new Size(1, height);
            listView.SmallImageList = imgList;
        }
        private void toOne(ListView l1, ListView l2)
        {
            try
            {
                // MessageBox.Show("Привет" + l2.SelectedIndices[0]);
                l1.Items.Clear();

                MessageBox.Show(l2.SelectedItems[0].Text);
                ListViewItem itm = new ListViewItem(Threat.toarrr(Threatdic[Convert.ToInt32(l2.SelectedItems[0].Text)]));
                l1.Items.Add(itm);
            }
            catch (Exception x)
            { MessageBox.Show(x.Message); }

        }
        private void listView2_ItemActivate(object sender, EventArgs e)
        {
            toOne(listView3, listView2);

        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            toOne(listView3, listView1);
        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            if (!File.Exists("C:\\IO\\ThreatListFSTEC.xlsx"))
            {
                try
                {
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                        "C:\\IO\\ThreatListFSTEC.xlsx");
                    MessageBox.Show("Файл в на жестком диске не обнаружен. Скачана последняя версия...");
                    ThreatParse2("C:\\IO\\ThreatListFSTEC.xlsx", Threatdic);
                    toList();
                }
                catch (Exception x) { MessageBox.Show(x.Message); }
            }
            else
            {
                try
                {
                    Form2 form2 = new Form2();

                    //MessageBox.Show(threatlist2.ElementAt(i).GetHashCode().ToString());
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", "C:\\IO\\ThreatListFSTECnew.xlsx");
                    List<Threat> threatlist3 = new List<Threat>();
                    ThreatParse2("C:\\IO\\ThreatListFSTECnew.xlsx", Threatdic1);
                    //MessageBox.Show(Threatdic.Count.ToString());
                    // MessageBox.Show(Threatdic1.Count.ToString());


                    foreach (KeyValuePair<int, Threat> box in Threatdic)
                    {
                        if (Threatdic1.ContainsKey(box.Key))
                        {
                            if (Threatdic1[box.Key].GetHashCode() == Threatdic[box.Key].GetHashCode())
                            { }
                            else
                            {
                                ListViewItem itm = new ListViewItem(Threat.toarrr(Threatdic[box.Key]));

                                form2.listView4.Items.Add(itm);
                                ListViewItem itm1 = new ListViewItem(Threat.toarrr(Threatdic1[box.Key]));

                                form2.listView6.Items.Add(itm1);
                            }

                        }
                        else
                        {


                            form2.listView4.Items.Add(new ListViewItem(Threat.toarrr(Threatdic[box.Key])));
                        }
                    }
                    foreach (KeyValuePair<int, Threat> box in Threatdic1)
                    {
                        if ((Threatdic1.ContainsKey(box.Key)) == false)
                        {
                            form2.listView6.Items.Add(new ListViewItem(Threat.toarrr(Threatdic1[box.Key])));
                        }
                    }
                    Threatdic = Threatdic1;
                    //  MessageBox.Show(Threatdic.ElementAt(5).ToString());

                    Threatdic1.Clear();
                    listView1.Items.Clear();
                    listView2.Items.Clear();
                    form2.Show();

                    File.Delete("C:\\IO\\ThreatListFSTEC.xlsx");
                    File.Move("C:\\IO\\ThreatListFSTECnew.xlsx", "C:\\IO\\ThreatListFSTEC.xlsx");

                }
                catch (Exception x) { MessageBox.Show(x.Message); }
            }
            toList();
        }




        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Threatdic.Clear();
            if (!File.Exists("C:\\IO\\ThreatListFSTEC.xlsx"))
            {
                try
                {
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                        "C:\\IO\\ThreatListFSTEC.xlsx");
                    MessageBox.Show("Файл в на жестком диске не обнаружен. Скачана последняя версия...");
                    ThreatParse2("C:\\IO\\ThreatListFSTEC.xlsx", Threatdic);
                    toList();
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
            else
            {
                try
                {
                    threatlist2.Clear();
                    ThreatParse2("C:\\IO\\ThreatListFSTEC.xlsx", Threatdic); toList();

                }
                catch (Exception x) { MessageBox.Show(x.Message); }
            }
        }





        private void button3_Click(object sender, EventArgs e)
        {
            if ((textBox2.Text == null) || (!textBox2.Text.Contains(".xlsx"))) { MessageBox.Show("Введите путь для сохранения файла с указанием расхирения .xlsx"); }
            else { 
            if (Threatdic.Count > 0)
            {

                try
               {
                    using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(textBox2.Text)))
                    {
                        xlPackage.Workbook.Worksheets.Add("ThreatFSTEC");
                        var myWorksheet = xlPackage.Workbook.Worksheets.Last();

                        for (int x = 1; x < Threatdic.Count; x++)
                        {
                            for (int y = 1; y < 8; y++)
                            {
                                myWorksheet.Cells[x, y].Value = Threat.toarrr(Threatdic.ElementAt(x).Value)[y-1].ToString();
                            }
                        }
                        xlPackage.Save();
                    }
               }
                catch (Exception x) { MessageBox.Show(x.Message); }
            }  




            else { MessageBox.Show("Загрузите сначала локальную базу"); }}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            if (sv.ShowDialog() == DialogResult.Cancel)
            { }
            else { textBox2.Text = sv.FileName; }
        }


    }
}


