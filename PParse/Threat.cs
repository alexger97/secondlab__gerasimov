﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSTECParser
{
    public class Threat
    {
        public int Id { get; private set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public string Сonfidentiality { get; set; }
        public string Integrity { get; set; }
        public string Availability { get; set; }

        public Threat(int id, string threatName, string threatDescription, string threatSource,
            string threatObject, string сonfidentiality, string integrity, string availability)
        {
            Id = id;
            ThreatName = threatName;
            if (threatDescription.Contains("_x000d_")){ threatDescription= threatDescription.Replace("_x000d_", ""); }
            ThreatDescription = threatDescription;
            ThreatSource = threatSource;
            ThreatObject = threatObject;
            if (сonfidentiality.Contains("1"))
            { Сonfidentiality = "Да"; }
            else { Сonfidentiality = "Нет"; }

            if (integrity.Contains("1"))
            { Integrity = "Да"; }
            else { Integrity = "Нет"; }

            if (availability.Contains("1"))
            { Availability = "Да"; }
            else { Availability = "Нет"; }

           
        }

        public override bool Equals(object obj)
        {
            var threat = obj as Threat;
            return threat != null &&
                   Id == threat.Id &&
                   ThreatName == threat.ThreatName &&
                   ThreatDescription == threat.ThreatDescription &&
                   ThreatSource == threat.ThreatSource &&
                   ThreatObject == threat.ThreatObject &&
                   Сonfidentiality == threat.Сonfidentiality &&
                   Integrity == threat.Integrity &&
                   Availability == threat.Availability;
        }

        public override int GetHashCode()
        {
            var hashCode = 1156073086;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatDescription);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatSource);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatObject);
            hashCode = hashCode * -1521134295 + Сonfidentiality.GetHashCode();
            hashCode = hashCode * -1521134295 + Integrity.GetHashCode();
            hashCode = hashCode * -1521134295 + Availability.GetHashCode();
            return hashCode;
        }
        public  static string[] toarrr(Threat x)
        { string[] s = new string[8];
            s[0] = x.Id.ToString();
            s[1] = x.ThreatName;
            s[2] = x.ThreatDescription;
            s[3] = x.ThreatSource;
            s[4] = x.ThreatObject;
            s[5] = x.Сonfidentiality;
            s[6] = x.Integrity;
            s[7] = x.Availability;
            return s;

        }
    }
}
